import sys
import matplotlib
matplotlib.use('Agg')

import pandas as pd
import matplotlib.pyplot as plt

Months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct']
Accidents = [10, 15, 30, 25, 50, 60, 70, 80, 90, 100]
Rainy_Days = [5, 8, 12, 10, 15, 18, 20, 22, 25, 30]

data = {"Months": Months, "Accidents": Accidents, "Rainy_Days": Rainy_Days}
df = pd.DataFrame(data)


df.plot(x="Rainy_Days", y="Accidents", kind="scatter", title="Accidents vs. Rainy Days")
plt.xlabel("Rainy Days")
plt.ylabel("Accidents")


correlation_matrix = df.corr()
print("Correlation Matrix:")
print(correlation_matrix)


plt.savefig(sys.stdout.buffer)
sys.stdout.flush()

