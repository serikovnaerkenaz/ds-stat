import matplotlib
matplotlib.use('Agg')

import pandas as pd
import matplotlib.pyplot as plt

health_data = pd.read_csv("data.csv", header=0, sep=",")

health_data.plot(x ='Duration', y='Calorie_Burnage', kind='scatter'),
plt.show()

plt.savefig(sys.stdout.buffer)
sys.stdout.flush()
