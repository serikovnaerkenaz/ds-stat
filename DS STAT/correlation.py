import pandas as pd

full_health_data = pd.read_csv("data.csv", header=0, sep=",")

selected_columns = ['Max_Pulse', 'Average_Pulse']
selected_data = full_health_data[selected_columns]

correlation_matrix = round(selected_data.corr(), 2)

print(correlation_matrix)
