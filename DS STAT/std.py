import pandas as pd
import numpy as np

data = pd.read_csv("data.csv", header=0, sep=",")

stdhw = data["Hours_Work"]
std = np.std(stdhw)

print('Standard Deviation Hours Work:',std)
